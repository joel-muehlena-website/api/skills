import { NextFunction, Request, Response } from "express";

import { Skill } from "../../models/v2/Skill";
import { SkillGroup as ISkill } from "../../models/v2/Skill.interface";
import { editSkill } from "./editSkill";

export const createSkillGroup = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const skillGroupName = req.body.skillGroupName;

  const skill: Array<ISkill> = await Skill.find({ skillGroupName });

  if (skill.length >= 1) {
    editSkill(req, res, next);
    return;
  }

  const date = new Date();
  await new Skill({
    skillGroupName,
    skills: [],
    createdAt: date,
    updatedAt: date,
  }).save();

  editSkill(req, res, next);
};
