import { NextFunction, Request, Response } from "express";

import { Skill } from "../../models/v2/Skill";
import { SkillGroup as ISkill } from "../../models/v2/Skill.interface";

export const deleteSkillGroup = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params.id;

  const skill: ISkill = await Skill.findById(id);

  if (!skill) {
    res.status(404).json({
      code: 404,
      msg: `No skill with the id: ${id} found. Could not be deletet`,
    });
    return next();
  }

  const delData: ISkill = await Skill.findByIdAndDelete(id);

  console.log(delData);
  if (delData) {
    res.status(200).json({ code: 200, msg: "Success", data: delData });
    return next();
  } else {
    res
      .status(500)
      .json({ code: 500, msg: "Failed to delete the skill-group entry" });

    return next();
  }
};
