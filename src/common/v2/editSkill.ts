import { NextFunction, Request, Response } from "express";

import { Skill } from "../../models/v2/Skill";
import {
  SkillGroup as ISkill,
  Skill as ISkillPoint,
} from "../../models/v2/Skill.interface";

export const editSkill = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const skillGroupName = req.body.skillGroupName;
  const id = req.body.id;

  let skills: Array<ISkill>;
  const updatedSkill: any = {};

  if (id) {
    skills = await Skill.find({ _id: id });
    if (skillGroupName && skillGroupName != skills[0].skillGroupName) {
      updatedSkill.skillGroupName = skillGroupName;
    }
  } else {
    skills = await Skill.find({ skillGroupName });
  }

  if (skills.length <= 0) {
    res.status(404).json({
      code: 404,
      msg: `No skill with the group name: ${skillGroupName} found.`,
    });
    return next();
  } else if (skills.length > 1) {
    res.status(500).json({
      code: 500,
      msg: `Internal error: Too many entries with the group name: ${skillGroupName} found.`,
    });
    return next();
  }

  let skillList: Array<ISkillPoint> = [];

  if (req.body.skillData) {
    const jsonSkillData: { skills: Array<ISkillPoint> } = JSON.parse(
      req.body.skillData
    );
    jsonSkillData.skills.forEach((skill) => {
      skillList.push(skill);
    });
  }

  if (skillList != skills[0].skills) updatedSkill.skills = skillList;
  updatedSkill.updatedAt = new Date();

  const updatedSkillData = await Skill.findByIdAndUpdate(
    skills[0]._id,
    { $set: updatedSkill },
    { new: true }
  );

  if (updatedSkillData) {
    res
      .status(200)
      .json({ msg: 200, message: "Success", data: updatedSkillData });

    return next();
  } else {
    res
      .status(500)
      .json({ code: 500, msg: "Failed to update the skill entry" });

    return next();
  }
};
