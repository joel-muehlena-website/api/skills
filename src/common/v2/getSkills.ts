import { NextFunction, Request, Response } from "express";

import { Skill } from "../../models/v2/Skill";
import { SkillGroup as ISkill } from "../../models/v2/Skill.interface";

export const getSkills = async (
  _: Request,
  res: Response,
  next: NextFunction
) => {
  const skills: Array<ISkill> = await Skill.find();

  if (skills.length <= 0) {
    res.status(404).json({ code: 404, msg: "No skill entries found" });
    return next();
  }

  res.json({ code: 200, msg: "Success", data: skills });
  return next();
};

export const getSkillById = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params.id;

  const skill: ISkill = await Skill.findById(id);

  if (!skill) {
    res
      .status(404)
      .json({ code: 404, msg: `No skill with the id: ${id} found.` });
    return next();
  }

  res.json({ code: 200, msg: "Success", data: skill });
  return next();
};
