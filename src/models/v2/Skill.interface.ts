export interface SkillGroup {
  _id: string;
  skillGroupName: string;
  skills: Array<Skill>;
  createdAt: Date;
  updatedAt: Date;
}

export interface Skill {
  skillName: string;
  skillDescription?: string;
  level: number;
}
