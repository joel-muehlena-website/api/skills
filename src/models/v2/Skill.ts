import { Schema, model } from "mongoose";

const SkillSchema = new Schema({
  skillGroupName: {
    type: String,
    required: true,
  },
  skills: [
    {
      skillName: { type: String, required: true },
      skillDescription: { type: String, required: false },
      level: { type: Number, required: true },
    },
  ],
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

export const Skill = model("skillsV2", SkillSchema);
