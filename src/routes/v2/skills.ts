import { Router } from "express";
import { editSkill } from "../../common/v2/editSkill";
import { deleteSkillGroup } from "../../common/v2/deleteSkillGroup";
import { getSkillById, getSkills } from "../../common/v2/getSkills";
import { createSkillGroup } from "../../common/v2/createSkillGroup";

export const skillV2Router = Router();

skillV2Router.get("/skill", getSkills);
skillV2Router.get("/skill/:id", getSkillById);
skillV2Router.post("/skill", createSkillGroup);
skillV2Router.put("/skill", editSkill);
skillV2Router.delete("/skill/:id", deleteSkillGroup);
